package library.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Lecteur
 *
 */
@Entity

public class Lecteur implements Serializable {
	
	@Id
	@GeneratedValue
	private int id;
	
	private String nom;
	
	private String prenom;
	
	@OneToMany(fetch=FetchType.EAGER,cascade={CascadeType.REMOVE},mappedBy="lecteur")
	private List<Emprunt> emprunts = new ArrayList<Emprunt>();
	
	private static final long serialVersionUID = 1L;

	public Lecteur() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Emprunt> getEmprunts() {
		return emprunts;
	}

	public void setEmprunts(List<Emprunt> emprunts) {
		this.emprunts = emprunts;
	}
   
}
