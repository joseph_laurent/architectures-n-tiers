package library.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Livre
 *
 */
@Entity

public class Livre implements Serializable {
	
	@Id
	@GeneratedValue
	private int id;
	private String author;
	private String title;
	
	private static final long serialVersionUID = 1L;

	public Livre() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
   
}
