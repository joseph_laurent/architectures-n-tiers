import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import library.beans.*;
import library.entities.*;




public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Go !");
		Main client=new Main();
		client.test1();
	}

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public Main() {
		
	}

	private void test1() {
		// TODO Auto-generated method stub
		System.out.println("Go !");
		
		final Hashtable jndiProperties = new Hashtable();
		jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		jndiProperties.put(Context.SECURITY_PRINCIPAL,"user");
	    jndiProperties.put(Context.SECURITY_CREDENTIALS, "user123");
	    jndiProperties.put("jboss.naming.client.ejb.context", true);

	    try {
			Context context = new InitialContext(jndiProperties);
			
			GestionLivreRemote gestionLivres = (GestionLivreRemote)context.lookup("MyLibrary17/MyBeans17/GestionLivre!library.beans.GestionLivreRemote");
			
			Livre l = new Livre();
			l.setAuthor("Houellebecq");
			l.setTitle("La carte et le territoire");
			
			l = gestionLivres.ajouterLivre(l);
			
			System.out.println(l.getId());

			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LivreExistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}