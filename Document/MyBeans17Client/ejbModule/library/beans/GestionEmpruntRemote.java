package library.beans;

import javax.ejb.Remote;

import library.entities.Emprunt;
import library.entities.Lecteur;
import library.entities.Livre;

@Remote
public interface GestionEmpruntRemote {

	Emprunt consulterEmprunt(String titre);

	void validerEmprunt();

	void choisirLivre(Livre l);

	void choisirLecteur(Lecteur l);

}
