package library.beans;

import javax.ejb.Remote;
import library.entities.Livre;

@Remote
public interface GestionLivreRemote {
	
	public Livre ajouterLivre(Livre l) throws LivreExistant;
	
	public void supprimerLivre(Livre l) throws LivreInconnu;
	
	public void modifierlivre(Livre l) throws LivreInconnu;
	
	public Livre consulterLivre(String titre) throws LivreInconnu;

}
