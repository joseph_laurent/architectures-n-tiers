package library.beans;

import javax.ejb.Remote;

import library.entities.Lecteur;

@Remote
public interface GestionLecteurRemote {

	Lecteur consulterLecteur(int id);

	void modifierLecteur(Lecteur l);

	void supprimerLecteur(Lecteur l);

	Lecteur ajouterLecteur(Lecteur l);

}
