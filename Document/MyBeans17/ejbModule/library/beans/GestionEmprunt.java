package library.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import library.dao.DAOEmprunt;
import library.entities.Emprunt;
import library.entities.Lecteur;
import library.entities.Livre;

/**
 * Session Bean implementation class GestionEmprunt
 */
@Stateful
@LocalBean
public class GestionEmprunt implements GestionEmpruntRemote {
	
	@EJB
	DAOEmprunt dao;

    private Lecteur lecteur;
	private Livre livre;

	/**
     * Default constructor. 
     */
    public GestionEmprunt() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void choisirLecteur(Lecteur l)
    {
    	lecteur=l;
    }
    
    @Override
	public void choisirLivre(Livre l)
    {
    	livre=l;
    }
    
    @Override
	public void validerEmprunt()
    {
    	Emprunt e = new Emprunt();
    	e.setDateRetour(new Date());
    	e.setLecteur(lecteur);
    	e.setLivre(livre);
    	dao.ajouterEmprunt(e);
    }
    
    @Override
	public Emprunt consulterEmprunt(String titre)
    {
    	return dao.trouverEmprunt(titre);
    	
    }

}
