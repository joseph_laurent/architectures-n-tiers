package library.beans;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import library.dao.DAOLivre;
import library.entities.Livre;

/**
 * Session Bean implementation class GestionLivre
 */
@Stateless
@LocalBean
public class GestionLivre implements GestionLivreRemote {
	
	@EJB
	DAOLivre dao;

    /**
     * Default constructor. 
     */
    public GestionLivre() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Livre ajouterLivre(Livre l) throws LivreExistant {
		// TODO Auto-generated method stub
		l = dao.insererLivre(l);
		return l;
	}

	@Override
	public void supprimerLivre(Livre l) throws LivreInconnu {
		// TODO Auto-generated method stub
		dao.supprimerLivre(l);
	}

	@Override
	public void modifierlivre(Livre l) throws LivreInconnu {
		// TODO Auto-generated method stub
		dao.modifierLivre(l);
	}

	@Override
	public Livre consulterLivre(String titre) throws LivreInconnu {
		// TODO Auto-generated method stub
		return dao.consulterLivre(titre);
	}

}
