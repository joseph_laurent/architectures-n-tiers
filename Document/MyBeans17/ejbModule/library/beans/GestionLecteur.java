package library.beans;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import library.dao.DAOLecteur;
import library.entities.Lecteur;

/**
 * Session Bean implementation class GestionLecteur
 */
@Stateless
@LocalBean
public class GestionLecteur implements GestionLecteurRemote {
	
	@EJB
	DAOLecteur dao;

    /**
     * Default constructor. 
     */
    public GestionLecteur() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public Lecteur ajouterLecteur(Lecteur l)
    {
     return dao.ajouterLecteur(l);
    }
    
    @Override
	public void supprimerLecteur(Lecteur l)
    {
    	dao.supprimerLecteur(l);
    }
    
    @Override
	public void modifierLecteur(Lecteur l)
    {
    	dao.modifierLecteur(l);
    }
    
    @Override
	public Lecteur consulterLecteur(int id)
    {
    	return dao.trouverLecteur(id);
    }

}
