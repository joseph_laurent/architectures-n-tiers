package library.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import library.entities.Emprunt;

/**
 * Session Bean implementation class DAOEmprunt
 */
@Stateless
@LocalBean
public class DAOEmprunt {
	
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public DAOEmprunt() {
        // TODO Auto-generated constructor stub
    }

	public void ajouterEmprunt(Emprunt e) {
		// TODO Auto-generated method stub
		em.persist(e);
	}

	public Emprunt trouverEmprunt(String titre) {
		// TODO Auto-generated method stub
		TypedQuery<Emprunt> q=em.createQuery("select e from Emprunt e where e.livre.title = '"+titre+"'",Emprunt.class);
		return q.getResultList().get(0);
	}

}
