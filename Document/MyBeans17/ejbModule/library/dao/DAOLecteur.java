package library.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import library.entities.Lecteur;

/**
 * Session Bean implementation class DAOLecteur
 */
@Stateless
@LocalBean
public class DAOLecteur {
	
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public DAOLecteur() {
        // TODO Auto-generated constructor stub
    }

	public Lecteur ajouterLecteur(Lecteur l) {
		// TODO Auto-generated method stub
		em.persist(l);
		return l;
	}

	public void supprimerLecteur(Lecteur l) {
		// TODO Auto-generated method stub
		l = em.find(Lecteur.class, l.getId());
		em.remove(l);
	}

	public void modifierLecteur(Lecteur l) {
		// TODO Auto-generated method stub
		em.merge(l);
	}

	public Lecteur trouverLecteur(int id) {
		// TODO Auto-generated method stub
		return em.find(Lecteur.class, id);
	}
	
	

}
