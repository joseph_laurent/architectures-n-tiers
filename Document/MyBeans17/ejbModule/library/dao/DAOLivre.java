package library.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import library.beans.LivreExistant;
import library.beans.LivreInconnu;
import library.entities.Livre;

@Stateless
@LocalBean
public class DAOLivre {
	
	@PersistenceContext
	private EntityManager em;
	
	
	public Livre insererLivre(Livre l) throws LivreExistant
	{
		try
		{
		em.persist(l);
		}
		catch (PersistenceException e)
		{
			e.printStackTrace();
			throw new LivreExistant();
		}
		return l;
	}
	
	public Livre consulterLivre(int id) throws LivreInconnu
	{
		Livre l = em.find(Livre.class, id);
		
		if (l==null) throw new LivreInconnu();
		else return l;
	}
	
	public Livre consulterLivre(String titre) throws LivreInconnu
	{
		
		TypedQuery<Livre> q = em.createQuery("select l from Livre l where l.title = \'"+titre+"\'", Livre.class);
		
		Livre l = q.getResultList().get(0);
		
		if (l==null) throw new LivreInconnu();
		else return l;
	}
	
	public void supprimerLivre(Livre l) throws LivreInconnu
	{
			l = em.find(Livre.class,l.getId());
			if (l==null) throw new LivreInconnu();
			em.remove(l);
	}
	
	public void modifierLivre(Livre l) throws LivreInconnu
	{
			Livre m = em.find(Livre.class,l.getId());
			if (l==null) throw new LivreInconnu();
			em.merge(l);
	}


}
