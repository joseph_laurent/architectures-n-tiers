package entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Emprunt
 *
 */
@Entity

public class Emprunt implements Serializable {
	
	@Id
	@GeneratedValue
	private int id;
	
	@OneToOne
	private Livre livre;
	
	@ManyToOne
	private Lecteur lecteur;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	private java.util.Date dateRetour;
	
	public java.util.Date getDateRetour() {
		return dateRetour;
	}

	public void setDateRetour(java.util.Date dateRetour) {
		this.dateRetour = dateRetour;
	}

	private static final long serialVersionUID = 1L;

	public Emprunt() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Livre getLivre() {
		return livre;
	}

	public void setLivre(Livre livre) {
		this.livre = livre;
	}

	public Lecteur getLecteur() {
		return lecteur;
	}

	public void setLecteur(Lecteur lecteur) {
		this.lecteur = lecteur;
	}
   
}
