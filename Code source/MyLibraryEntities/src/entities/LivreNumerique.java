package entities;

import java.io.Serializable;
import javax.persistence.*;
import entities.Livre;

/**
 * Entity implementation class for Entity: LivreNumerique
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class LivreNumerique extends Livre implements Serializable {
	
	private Format format;
	
	private static final long serialVersionUID = 1L;

	public LivreNumerique() {
		super();
	}

	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}
   
}
